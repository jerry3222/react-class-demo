import React, { Component } from "react";
import "./App.css";
import Tvdisplay from "./components/tvdisplay";
import { Tvs } from "./db";
import { Tvinput } from "./components/tvinput";
import { Alert } from "./components/alert.js";

// const Tvs = require('./db');
class App extends Component {
  state = {
    tvArr: Tvs,
    brand: "Vizio",
    size: 65,
    piclink:
      "https://images.costco-static.com/ImageDelivery/imageService?profileId=12026539&itemId=9650165-894&recipeName=680",
    description: "Vizio Quantum X PX65-G1 65-in. Smart 4K HDR LED TV",
    price: 1799.99,
    rating: 4,
    alert: {
      show: false,
      type: "",
      text: "",
    },
  };

  handleChange = (e) => {
    // console.log(e.target.value);

    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  handelAlert = ({ type, text }) => {
    this.setState({ alert: { show: true, type, text } });

    setTimeout(() => {
      this.setState({ alert: { show: false } });
    }, 3000);
  };

  handleSubmit = (e) => {
    e.preventDefault();
    const {
      brand,
      size,
      piclink,
      description,
      price,
      rating,
      tvArr,
    } = this.state;
    if (
      brand !== "" &&
      size > 0 &&
      piclink !== "" &&
      description !== "" &&
      price > 0 &&
      rating > 0
    ) {
      let tempTvArr = [
        ...tvArr,
        {
          id: tvArr.length + 1,
          brand,
          size,
          piclink,
          description,
          price: parseFloat(price),
          rating: parseInt(rating),
        },
      ];
      this.setState({ tvArr: tempTvArr });
      this.handelAlert({type: "success", text: "Added successful"})
      // console.log(tempTvArr);
    } else {
      console.log("error");
      this.handelAlert({ type: "danger", text: "Enter correctly" });
    }
  };

  render() {
    const {
      brand,
      size,
      piclink,
      description,
      price,
      rating,
      alert,
    } = this.state;
    return (
      <div className="container">
        {alert.show && <Alert type={alert.type} text={alert.text} />}
        <Tvinput
          handleChange={this.handleChange}
          handleSubmit={this.handleSubmit}
          brand={brand}
          size={size}
          description={description}
          piclink={piclink}
          price={price}
          rating={rating}
        />
        <div className="row">
          {this.state.tvArr.map((tv, index) => (
            <Tvdisplay key={index} value={tv} />
          ))}
        </div>
      </div>
    );
  }
}

export default App;
