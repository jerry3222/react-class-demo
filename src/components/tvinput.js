import React from "react";

export const Tvinput = ({brand, size, description, piclink, price, rating, handleChange, handleSubmit}) => {
  return (
    <div>
      <form onSubmit={handleSubmit}>
        <div className="card border-secondary mb-3">
          <div className="card-header">TV Input</div>
          <div className="card-body text-secondary">
            <div className="form-row">
              <div className="form-group col-md-6">
                <label htmlFor="brand">Brand</label>
                <input
                  type="text"
                  className="form-control"
                  id="brand"
                  name="brand"
                  placeholder="Brand"
                  value={brand}
                  onChange={(e) => handleChange(e)}
                />
              </div>
              <div className="form-group col-md-6">
                <label htmlFor="size">Size</label>
                <input
                  type="number"
                  className="form-control"
                  id="size"
                  placeholder="Size"
                  name="size"
                  value={size}
                  onChange={(e) => handleChange(e)}
                />
              </div>
            </div>
            <div className="form-group">
              <label htmlFor="description">Description</label>
              <input
                type="text"
                className="form-control"
                id="description"
                placeholder="Description"
                name="description"
                value={description}
                  onChange={(e) => handleChange(e)}
              />
            </div>
            <div className="form-group">
              <label htmlFor="piclink">Pic Link</label>
              <input
                type="text"
                className="form-control"
                id="piclink"
                placeholder="Pic http link"
                name="piclink"
                value={piclink}
                  onChange={(e) => handleChange(e)}
              />
            </div>
            <div className="form-row">
              <div className="form-group col-md-6">
                <label htmlFor="price">Price</label>
                <div className="input-group mb-3">
                  <div className="input-group-prepend">
                    <span className="input-group-text">$</span>
                  </div>
                  <input
                    type="number"
                    className="form-control"
                    id="price"
                    aria-label="Amount (to the nearest dollar)"
                    name="price"
                    value={price}
                  onChange={(e) => handleChange(e)}
                  />
                  <div className="input-group-append">
                    <span className="input-group-text">.00</span>
                  </div>
                </div>
              </div>
              <div className="form-group col-md-6">
                <label htmlFor="rating">Ratings</label>
                <input
                  type="number"
                  className="form-control"
                  id="rating"
                  name="rating"
                  value={rating}
                  onChange={(e) => handleChange(e)}
                />
              </div>
            </div>

            <button
              type="submit"
              className="btn btn-primary btn-block"
              id="btn"
            >
              Add TV
            </button>
          </div>
        </div>
      </form>
    </div>
  );
};
