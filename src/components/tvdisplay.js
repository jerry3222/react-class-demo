import React from 'react'
import {Tvpic, Tvsound} from './tvpic';
import {Tvtitle} from './tvtitle';
import Tvdetail from './tvdetail';

const Tvdisplay = (props) => {
    console.log(props);
    
    return (
        <div className="card m-3" style={{ width: "18rem" }}>
          <Tvpic value={props.value.piclink} />
          <div className="card-body">
            <Tvtitle brand={props.value.brand} size={props.value.size} price={props.value.price} />
            <Tvdetail description={props.value.description} rating={props.value.rating} />
          </div>
        </div>
    )
}

export default Tvdisplay