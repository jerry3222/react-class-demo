import React from "react";
import "./alert.css";

export const Alert = ({ type, text }) => {
  return <div id="alert-div" className={`alert alert-${type}`}>{text}</div>;
};
