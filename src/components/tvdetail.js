import React from "react";

const Tvdetail = (props) => {
  return (
    <div>
      <p className="card-text">{props.description}</p>
      <p className="card-text">
        <font color="#ddd">
          {Array.from(Array(props.rating), (e, i) => (
            <span key={i}>★</span>
          ))}
        </font>
      </p>
    </div>
  );
};

export default Tvdetail;
