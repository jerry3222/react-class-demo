import React from "react";

export const Tvtitle = (props) => (
  <h5 className="card-title">
    {props.brand} {props.size}{" "}
    <strong className="float-right">${props.price}</strong>
  </h5>
);