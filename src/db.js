export const Tvs = [
    {
        id: 1,
        brand: "Sony",
        size: "85",
        description: "Sony 85-in. 4K HDR Android Smart TV XBR85X950H",
        piclink:
          "https://images.costco-static.com/ImageDelivery/imageService?profileId=12026539&itemId=9099585-894&recipeName=680",
        price: 5998,
        rating: 5,
      },
      {
        id: 2,
        brand: "Samsung",
        size: "55",
        description: 'Samsung 55" 4K UHD HDR LED Tizen Smart TV (UN55RU7100FXZC)',
        piclink:
          "https://images.costco-static.com/ImageDelivery/imageService?profileId=12026539&imageId=5106065-894__1&recipeName=350",
        price: 2200,
        rating: 4,
      },
      {
        id: 3,
        brand: "TCL",
        size: "55",
        description:
          'TCL 4-Series 55" 4K UHD HDR LED Roku OS Smart TV (55S425-CA)',
        piclink:
          "https://images.costco-static.com/ImageDelivery/imageService?profileId=12026539&imageId=4236155-894__1&recipeName=350",
        price: 399,
        rating: 3,
      },
   
]